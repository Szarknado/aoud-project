import ast
from functools import reduce
from multiprocessing import Pool
import os

import pandas as pd
from PIL import Image
import requests


def get_not_downloaded_photos(f: str):
    if not f.endswith('.csv'):
        return []
    print(f)
    df = pd.read_csv(os.path.join('../data', f))
    date = f[5:-4]
    os.makedirs(os.path.join('../image_data', date), exist_ok=True)
    return [(os.path.join('../image_data', date, f"image_{id}_{i}.png"), p_url)
            for id, photos in df[['id', 'photos']].itertuples(index=False)
            for i, p_url in enumerate(ast.literal_eval(photos))
            if not os.path.exists(os.path.join('../image_data', date, f"image_{id}_{i}.png"))]


def process_single_photo(f: str, p_url: str):
    try:
        Image.open(requests.get(p_url, stream=True).raw).save(f)
    except:
        print(p_url)


if __name__ == '__main__':
    not_downloaded_photos = []
    for _, _, files in os.walk('../data'):
        print(f"Processing {len(files)} files")
        with Pool() as p:
            not_downloaded_photos += p.map(get_not_downloaded_photos, files)
    not_downloaded_photos = reduce(lambda x, y: x + y, not_downloaded_photos)
    print(f"Downloading {len(not_downloaded_photos)} missing photos")
    with Pool() as p:
        p.starmap(process_single_photo, not_downloaded_photos)
