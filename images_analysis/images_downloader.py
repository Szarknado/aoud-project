import ast
from multiprocessing import Pool
import os

import pandas as pd
from PIL import Image
import requests


def process_single_file(f: str):
    if not f.endswith('.csv') or f:
        return
    print("Start", f)
    os.makedirs(os.path.join('../image_data', f[5:-4]), exist_ok=True)
    df = pd.read_csv(os.path.join('../data', f))
    for id, photos in df[['id', 'photos']].itertuples(index=False):
        for i, p_url in enumerate(ast.literal_eval(photos)):
            try:
                Image.open(
                    requests.get(p_url, stream=True).raw
                ).save(
                    os.path.join('../image_data', f[5:-4], f"image_{id}_{i}.png")
                )
            except:
                print(p_url)
    print("End", f)


if __name__ == '__main__':
    for _, _, files in os.walk('../data'):
        with Pool() as p:
            p.map(process_single_file, files)
