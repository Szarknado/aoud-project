import ast
from multiprocessing import Pool
import numpy as np
import os
import pandas as pd
from pathlib import Path

from PIL import Image


def calculate_distance_to_gray_picture(image):
    image = image.convert('RGB')
    gray_image_np = np.concatenate([np.asarray(image.convert('L')).astype(float)[..., np.newaxis]] * 3, -1) / 255.
    image_np = np.asarray(image).astype(float) / 255.
    return np.mean(np.abs(image_np - gray_image_np))


def calculate_distance_to_bw_picture(image):
    image = image.convert('RGB')
    bw_image_np = np.concatenate([np.asarray(image.convert('1')).astype(float)[..., np.newaxis]] * 3, -1)
    image_np = np.asarray(image).astype(float) / 255.
    return np.mean(np.abs(image_np - bw_image_np))


def analyze_single_file(f):
    if not f.endswith('.csv'):
        return
    print("Start", f)
    df_path = Path() / 'data' / f
    df = pd.read_csv(df_path)
    df['photos'] = df['photos'].apply(ast.literal_eval)
    df['photos_gray'] = [[]] * df.shape[0]
    df['photos_bw'] = [[]] * df.shape[0]
    date = f[5:-4]
    for i, t_id, photos in df.loc[df['photos'].apply(len) > 0, ['id', 'photos']].itertuples():
        read_photos = []
        for p_id, p in enumerate(photos):
            img_path = Path() / 'image_data' / date / f'image_{t_id}_{p_id}.png'
            try:
                img = Image.open(img_path)
            except:
                img = None
            read_photos.append(img)
        df.at[i, 'photos_gray'] = [None if img is None else calculate_distance_to_gray_picture(img) for img in
                                   read_photos]
        df.at[i, 'photos_bw'] = [None if img is None else calculate_distance_to_bw_picture(img) for img in read_photos]
    df.to_csv(df_path, index=False)
    print("End", f)


if __name__ == '__main__':
    for _, _, files in os.walk('../data'):
        with Pool() as p:
            p.map(analyze_single_file, [f for f in files if f not in ['data_2020-11-13.csv', 'data_2020-11-14.csv']])
