from datetime import timedelta
import pandas as pd
import twint


if __name__ == '__main__':
    # start_date, end_date = '2019-11-01', '2019-11-30'
    # start_date, end_date = '2019-12-01', '2019-12-31'
    # start_date, end_date = '2020-01-01', '2020-01-31'
    # start_date, end_date = '2020-02-01', '2020-02-29'
    # start_date, end_date = '2020-03-01', '2020-03-31'
    # start_date, end_date = '2020-04-01', '2020-04-30'
    # start_date, end_date = '2020-05-01', '2020-05-31'
    # start_date, end_date = '2020-06-01', '2020-06-30'
    # start_date, end_date = '2020-07-01', '2020-07-31'
    # start_date, end_date = '2020-08-01', '2020-08-31'
    # start_date, end_date = '2020-09-01', '2020-09-30'
    # start_date, end_date = '2020-10-01', '2020-10-31'
    # start_date, end_date = '2020-11-01', '2020-11-30'
    # start_date, end_date = '2020-12-01', '2020-12-20'
    date_range = [(str(i.date()), str((i + timedelta(days=1)).date()))
                  for i in pd.date_range(start=start_date, end=end_date, freq='D')]

    for since, until in date_range:
        output_file = f"data/data_{since}.csv"
        print(since)

        c = twint.Config()
        # c.Near = city
        # c.Geo = geo
        c.Since = since
        c.Until = until
        c.Search = '#unusannus OR #unusanus OR ' \
                   '#mementomori OR #momentomori OR ' \
                   '#wewerehere OR ' \
                   '#unusannusfanart OR ' \
                   '#unusannusisoverparty OR ' \
                   '#unusannusoverparty OR ' \
                   '#unusannusmemes OR ' \
                   '#ripunusannus OR '\
                   '#crankgameplays OR #markiplier OR ' \
                   '@crankgameplays OR @markiplier'
        c.Lang = 'en'
        c.Store_csv = True
        c.Output = output_file
        twint.run.Search(c)
